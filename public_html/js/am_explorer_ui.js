
//var ExplorerUi = React.createClass(
class ExplorerUi extends React.Component
{	
	constructor(props)
	{
		super(props);
		this.state = { focusedEntity : {firstname:"Moo"} };
	}
	
	componentDidMount()
	{
		console.log("[ExplorerUi] created");
		MESSENGER.registerListener(this, "focus");
	}
	
	handleMessage(m, messageType)
	{
		// Set new state for overlay.
		if (messageType == "focus")
        {
            this.setState({focusedEntity:m});
        }
	}
	
	componentWillUnmount()
	{
		//
	}
	
    render()
	{
		// {this.state.focusedEntity.firstname}
		return <div id="overlay">
			{this.state.focusedEntity.firstname + '\n'}
			<ObjectDisp value={this.state.focusedEntity}/>
		</div>;
    }
}



class ObjectDisp extends React.Component
{	
	constructor(props)
	{
		super(props);
		this.state = { entity : props.value };
	}
	
	componentDidMount()
	{
		console.log("[ExplorerUi] created");
		MESSENGER.registerListener(this, "focus");
	}
	
	handleMessage(m, messageType)
	{
		// "focus" updates the parent panel, this may not respond to events at all except UI
		if (messageType == "focus")
        {
            //
        }
	}
	
	componentWillReceiveProps(props)
	{
		this.setState({ entity : props.value });
	}

	componentWillUnmount()
	{
		//
	}
	
	getOrderedKeys(o)
	{
		var a = [];
		for (var p in o)
		{
			if (o.hasOwnProperty(p))
				a.push(p);
		}
		a.sort();
		return a;
	}

    render()
	{
		var o = this.state.entity;
		a = this.getOrderedKeys(o)

		// This just barfs up some text for each k/v
		// TODO
		// Write PropertyDisp!
		return <div>
			{
				a.map( function(i) {
					return <span key={i}>{i + ": " + o[i]}<br/></span>;
				})
			}
		</div>;
    }
}






ReactDOM.render(
    <ExplorerUi/>,
    //document.getElementById('webglWindow') // content
	document.getElementById('overlaymoo')
	//document.body
);