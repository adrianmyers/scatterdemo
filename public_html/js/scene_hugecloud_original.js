var camera, scene, renderer;
var scene_bg;
var camera_bg;
var geometry, mesh;
var controls;

var objects = [ ];

var amountOfParticles = 500000;
var maxDistance = Math.pow( 120, 2 );
var positions, alphas, particles, _particleGeom;

var clock = new THREE.Clock();

var blocker = document.getElementById( 'blocker' );
var instructions = document.getElementById( 'instructions' );



function init() {

	camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1000000 );

	scene = new THREE.Scene();
	scene.fog = new THREE.FogExp2( 0x000000, 0.01 );

	scene_bg = new THREE.Scene();

	controls = new THREE.FirstPersonControls( camera );
	controls.movementSpeed = 100;
	controls.lookSpeed = 0.1;



	// SKYBOX =========================================================================
//				var materials = [
//					new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( 'textures/cube/skybox/px.jpg' ) } ), // right
//					new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( 'textures/cube/skybox/nx.jpg' ) } ), // left
//					new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( 'textures/cube/skybox/py.jpg' ) } ), // top
//					new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( 'textures/cube/skybox/ny.jpg' ) } ), // bottom
//					new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( 'textures/cube/skybox/pz.jpg' ) } ), // back
//					new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( 'textures/cube/skybox/nz.jpg' ) } )  // front
//				];
//				
//				mesh = new THREE.Mesh( new THREE.BoxGeometry( 10000, 10000, 10000, 7, 7, 7 ), new THREE.MeshFaceMaterial( materials ) );
//				mesh.scale.x = - 1;
//				scene.add(mesh);


	// SCENE BG QUAD
	var mat_bg = new THREE.MeshBasicMaterial( {map: THREE.ImageUtils.loadTexture( 'textures/vignette_bg.jpg' )} );
	mat_bg.side = THREE.DoubleSide;
	var mesh_bg = new THREE.Mesh( new THREE.PlaneGeometry( 1, 1, 1, 1 ), mat_bg );
	scene_bg.add( mesh_bg );
	camera_bg = new THREE.OrthographicCamera( -0.5, 0.5, 0.5, -0.5, 0.1, 100 );
	camera_bg.position.z = -2;
	camera_bg.lookAt( new THREE.Vector3( 0, 0, 0 ) );


	// RENDERER =========================================================================
	renderer = new THREE.WebGLRenderer(); // Detector.webgl? new THREE.WebGLRenderer(): new THREE.CanvasRenderer()
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );

	renderer.autoClear = false;

	document.body.appendChild( renderer.domElement );
	
	var v = new THREE.Vector3(0,0,0);






	// SHADER =========================================================================
	var imagePreviewTexture = THREE.ImageUtils.loadTexture( 'textures/ClusterDot_32.png' );
	imagePreviewTexture.minFilter = THREE.LinearMipMapLinearFilter;
	imagePreviewTexture.magFilter = THREE.LinearFilter;

	// Trying the built-in PointsMaterial to fix some odd alpha sampling issues.
	// Note that FOG is basically standing in for the depth-based fadeout I'd like, but only works with a solid color BG.
	// Although I suppose a screen-side vignette post effect is easy enough since the performance difference is minimal in practice.
	pointShaderMaterial = new THREE.PointsMaterial( {
		size: 1.0,
		map: imagePreviewTexture,
		transparent: true,
		alphaTest: 0.1,
		fog: true
	} );

	// If restoring this, consider making it a RawShaderMaterial
//				pointShaderMaterial = new THREE.ShaderMaterial( {
//					uniforms: {
//						tex1: { type: "t", value: imagePreviewTexture },
//						zoom: { type: 'f', value: 9.0 }
//					},
//					vertexShader:   document.getElementById( 'vs_particle_simple' ).textContent,
//					fragmentShader: document.getElementById( 'ps_particle_simple' ).textContent,
//					transparent: true,
//					blending: 'NormalBlending',
//					blendSrc: 'SrcAlphaSaturateFactor',
//					blendDst: 'OneMinusSrcAlphaFactor',
//					blendEquation: 'AddEquation',
//					alphaTest: 0.1
//				});



	// PARTICLE CLOUD =========================================================================
	//create particles with buffer geometry
	var distanceFunction = function ( a, b ) {
		return Math.pow( a[0] - b[0], 2 ) + Math.pow( a[1] - b[1], 2 ) + Math.pow( a[2] - b[2], 2 );
	};

	positions = new Float32Array( amountOfParticles * 3 );
	alphas = new Float32Array( amountOfParticles );

	_particleGeom = new THREE.BufferGeometry();
	_particleGeom.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) );
	_particleGeom.addAttribute( 'alpha', new THREE.BufferAttribute( alphas, 1 ) );

	particles = new THREE.Points( _particleGeom, pointShaderMaterial );

	for ( var x = 0; x < amountOfParticles; x++ ) {
		positions[ x * 3 + 0 ] = Math.random() * 1000;
		positions[ x * 3 + 1 ] = Math.random() * 1000;
		positions[ x * 3 + 2 ] = Math.random() * 1000;
		alphas[x] = 1.0;
	}



	// K-D TREE =========================================================================
	var measureStart = new Date().getTime();
	kdtree = new THREE.TypedArrayUtils.Kdtree( positions, distanceFunction, 3 );
	console.log( 'TIME building kdtree', new Date().getTime() - measureStart );



	// GO GO GO!
	scene.add( particles );
	window.addEventListener( 'resize', onWindowResize, false );
}



function onWindowResize()
{
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );
	controls.handleResize();
}



function onRenderScene()
{
	// Currently disabled since it isn't doing anything at the moment, but it should!
	//displayNearest(camera.position);

	controls.update( clock.getDelta() );

	renderer.clear();
	renderer.render( scene_bg, camera_bg );

//				renderer.clear();		
	renderer.clearDepth();
	renderer.render( scene, camera );

	requestAnimationFrame( onRenderScene );
}



// Cool shit but currently unused.
function displayNearest( position )
{
	// take the nearest N around camera. distance^2 'cause we use the manhattan distance and no square is applied in the distance function.
	var imagePositionsInRange = kdtree.nearest( [ position.x, position.y, position.z ], 100, maxDistance );

	// We combine the nearest neighbour with a view frustum. Doesn't make sense if we change the sprites not in our view... well maybe it does. Whatever you want.
	var _frustum = new THREE.Frustum();
	var _projScreenMatrix = new THREE.Matrix4();
	camera.matrixWorldInverse.getInverse( camera.matrixWorld );

	_projScreenMatrix.multiplyMatrices( camera.projectionMatrix, camera.matrixWorldInverse );
	_frustum.setFromMatrix( _projScreenMatrix );

	// Buffer-based alpha update
	// Currently commented out as it never did work with the provided shader.
	// In the demo code on the main site, it's actually controlling out.r, probably because of an API change that made the alpha work differently.
	// Currently using a stock shader with a FOG effect to simulate the alpha change on a solid color bg.
	for ( i = 0, il = imagePositionsInRange.length; i < il; i++ )
	{
		var object = imagePositionsInRange[i];
		var objectPoint = new THREE.Vector3().fromArray( object[ 0 ].obj );

		if ( _frustum.containsPoint( objectPoint ) )
		{
			var objectIndex = object[0].pos;

			// set the alpha according to distance
			alphas[ objectIndex ] = 1.0 / maxDistance * object[1];
			_particleGeom.attributes.alpha.needsUpdate = true;
		}
	}
}



init();
onRenderScene();