//uniform float zoom;
attribute float alpha;
varying float vAlpha;

void main() {
	vAlpha = 1.0 - alpha;
	vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
	gl_PointSize = 4.0 * ( 300.0 / length( mvPosition.xyz ) );
	gl_Position = projectionMatrix * mvPosition;
}