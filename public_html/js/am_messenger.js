
var MESSENGER = { REVISION: '01' };

MESSENGER.listeners = {};

MESSENGER.registerListener = function (listener, messageType)
{
	if (!this.listeners.hasOwnProperty(messageType))
	{
		this.listeners[messageType] = [];
	}
	this.listeners[messageType].push(listener);
}

MESSENGER.send = function (m, messageType)
{
	if (this.listeners.hasOwnProperty(messageType))
	{
		for (var i=0; i<this.listeners[messageType].length; i++)
		{
			this.listeners[messageType][i].handleMessage(m, messageType);
		}
	}
}