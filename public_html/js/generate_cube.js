function generateCube(halfSize)
{
	var geometry = new THREE.BufferGeometry();
	
	var triangles = 12;
	
	var positions = new Float32Array( triangles * 3 * 3 );
	var normals = new Float32Array( triangles * 3 * 3 );
	var colors = new Float32Array( triangles * 3 * 3 );
	
	var color = new THREE.Color();
	
	var sides = ["+x", "-x", "+y", "-y", "+z", "-z"];
	var offsets = [
		new THREE.Vector3(halfSize, 0, 0),
		new THREE.Vector3(-halfSize, 0, 0),
		new THREE.Vector3(0, halfSize, 0),
		new THREE.Vector3(0, -halfSize, 0),
		new THREE.Vector3(0, 0, halfSize),
		new THREE.Vector3(0, 0, -halfSize)
	];
	for( var i=0; i<sides.length; i++ )
	{
		writeQuad(sides[i], halfSize, offsets[i], positions, normals, 3*3*i*2);
	}
	
	geometry.addAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) );
	geometry.addAttribute( 'normal', new THREE.BufferAttribute( normals, 3 ) );
	geometry.addAttribute( 'color', new THREE.BufferAttribute( colors, 3 ) );

	geometry.computeBoundingSphere();
		
	return geometry;
}



function writeQuad(axis, halfSize, offset, positions, normals, startIndex )
{
	// Assumes +z
	var v = new THREE.Vector3( 1, 1, 0 );
	var n = new THREE.Vector3( 0, 0, 1 );
	
	var ui = 0; // U index, 0=x
	var vi = 1;
	
	switch(axis)
	{
		case "-z":
			v.x = -1;
			//
			n.z = -1;
		break;
		
		case "+x":
			ui = 2;
			vi = 1;
			v.z = 1;
			//
			n.x = 1;
			n.z = 0;
		break;
		
		case "-x":
			ui = 2;
			vi = 1;
			v.x = 0;
			v.z = -1;
			//
			n.x = -1;
			n.z = 0;
		break;
		
		case "+y":
			ui = 0;
			vi = 2;
			v.y = 0;
			v.z = 1;
			//
			n.x = 0;
			n.y = 1;
			n.z = 0;
		break;
		
		case "-y":
			ui = 0;
			vi = 2;
			v.y = 0;
			v.z = -1;
			//
			n.x = 0;
			n.y = -1;
			n.z = 0;
		break;
	}
	
	scaleVector3(v, halfSize);
	
	// FACE A
	positions[ startIndex + 0  + ui  ] = -v.getComponent(ui);
	positions[ startIndex + 0  + vi  ] = -v.getComponent(vi);
	//
	positions[ startIndex + 3  + ui  ] = -v.getComponent(ui);
	positions[ startIndex + 3  + vi  ] = v.getComponent(vi);
	//
	positions[ startIndex + 6  + ui  ] = v.getComponent(ui);
	positions[ startIndex + 6  + vi  ] = v.getComponent(vi);
	
	// FACE B
	positions[ startIndex + 9  + ui  ] = -v.getComponent(ui);
	positions[ startIndex + 9  + vi  ] = -v.getComponent(vi);
	//
	positions[ startIndex + 12 + ui  ] = v.getComponent(ui);
	positions[ startIndex + 12 + vi  ] = v.getComponent(vi);
	//
	positions[ startIndex + 15 + ui  ] = v.getComponent(ui);
	positions[ startIndex + 15 + vi  ] = -v.getComponent(vi);
	
	for( var i=0; i<6; i++ )
	{
		positions[ startIndex + (i*3) + 0  ] += offset.x;
		positions[ startIndex + (i*3) + 1  ] += offset.y;
		positions[ startIndex + (i*3) + 2  ] += offset.z;
	}
	
	// FACE A
	normals[ startIndex + 0  ] = n.x;
	normals[ startIndex + 1  ] = n.y;
	normals[ startIndex + 2  ] = n.z;
	//
	normals[ startIndex + 3  ] = n.x;
	normals[ startIndex + 4  ] = n.y;
	normals[ startIndex + 5  ] = n.z;
	//
	normals[ startIndex + 6  ] = n.x;
	normals[ startIndex + 7  ] = n.y;
	normals[ startIndex + 8  ] = n.z;
	
	// FACE B
	normals[ startIndex + 9  ] = n.x;
	normals[ startIndex + 10 ] = n.y;
	normals[ startIndex + 11 ] = n.z;
	//
	normals[ startIndex + 12 ] = n.x;
	normals[ startIndex + 13 ] = n.y;
	normals[ startIndex + 14 ] = n.z;
	//
	normals[ startIndex + 15 ] = n.x;
	normals[ startIndex + 16 ] = n.y;
	normals[ startIndex + 17 ] = n.z;
}
