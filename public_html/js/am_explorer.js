var camera, scene, renderer;
var cameraT;
var raycaster;
var clock = new THREE.Clock();

var camWrapperTrans;
var camWrapperRotX;
var camWrapperRotY;

var windowDiv;
var windowDivOffsetX = 350; // Hardcoded to match webglOverlay CSS offset
var windowDivW;
var windowDivH;
var windowHalfX;
var windowHalfY;
var rayMouse = new THREE.Vector2();

var data_objects = [];
var data_mesh_positions = null; // Pos buffer
var data_mesh_ids = null; // Originally the data object ID, but since we have indices for the array, I'm not sure this is worth it (?)
var data_mesh_sizes = null; // NYI and more than likely isn't going to be, variable size turns out to require editing THREE for interactivity and sucks in practice anyway (scale variation is too big unless log)
var data_mesh_sh1 = null; // SH9 coeffs, only one channel as this is a greyscale/scalar effect, but it has to be split into 3 vec3 since THREE can't pass a mat3 or array as an Attribute :(
var data_mesh_sh2 = null;
var data_mesh_sh3 = null;
var data_mesh = null; // Geometry/Mesh, binding for the buffers
var data_GO = null; // Final Points object
var pointShaderMaterial = null;

var DEFAULT_POINT_SIZE = 1000.0;
var INTERACTIVE_POINT_SIZE = 0.45; // 1.25 works for a 2500.0 default point size, god alone knows why, something in ThreeJS innards.

var eventQueue = {
	events : [],
	handleMessage : function(m, messageType)
	{
        console.log("[Explorer] Data event queued.");
		this.events.push({body:m, type:messageType})
	}
};

function clamp(v, min, max)
{
	return Math.max(min, Math.min(v, max));
}
function clamp01(v)
{
	return Math.max(0.0, Math.min(v, 1.0));
}

// This function transposes the upper 3x3 block of a Matrix4
// The camera.matrixWorldInverse transpose is needed to convert a camera-space vector into worldspace
// I had a function that did this in the shader, but that's much more expensive that doing it once per frame as a uniform.
// Unfortunately, whatever ThreeJS's internal transpose function does, it doesn't work the way the shader version does.
// So I hand-rolled this explicit method, and it is stable and nice, but confusing and extremely context-specific.
function transpose3x3Matrix4(inMatrix)
{
	var outMatrix = new THREE.Matrix4();
	
	// column-major
	a = inMatrix.elements;
	
	// row-major
	outMatrix.set(
		a[0], a[1], a[2],  0,
		a[4], a[5], a[6],  0,
		a[8], a[9], a[10], 0,
		0,    0,    0,     1
	);
	
	return outMatrix;
}


function init()
{
	MESSENGER.registerListener(eventQueue, "data");
	windowDiv = document.getElementById("webglWindow");
	
	scene = new THREE.Scene();
	//scene.fog = new THREE.FogExp2( 0x000000, 0.01 );
	
	// RENDERER =========================================================================
	renderer = new THREE.WebGLRenderer({ antialias: true });
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize(windowDiv.clientWidth, windowDiv.clientHeight);
	renderer.autoClear = false;
	renderer.setClearColor(0x222222, 1); 
	//document.body.appendChild( renderer.domElement ); // webglWindow
	
	windowDiv.appendChild( renderer.domElement );
	document.addEventListener( 'mousemove', onDocumentMouseMove, false );
	document.addEventListener( 'keydown', onDocumentKeyDown, false );
	document.addEventListener( 'keyup', onDocumentKeyUp, false );
    windowDiv.addEventListener( 'mousedown', onDocumentMouseDown, false );
	windowDiv.addEventListener( 'mouseup', onDocumentMouseUp, false );
	
	// document.addEventListener( 'mousemove', onDocumentMouseMove, false );
	// document.addEventListener( 'keydown', onDocumentKeyDown, false );
	// document.addEventListener( 'keyup', onDocumentKeyUp, false );
    // document.addEventListener( 'mousedown', onDocumentMouseDown, false );
	// document.addEventListener( 'mouseup', onDocumentMouseUp, false );
	
	windowDivW = windowDiv.clientWidth;
	windowDivH = windowDiv.clientHeight;
	windowHalfX = windowDiv.clientWidth / 2;
	windowHalfY = windowDiv.clientHeight / 2;
	camera = new THREE.PerspectiveCamera( 60, windowDivW / windowDivH, 0.1, 1000 );
	
	camWrapperTrans = new THREE.Object3D();
	camWrapperRotX = new THREE.Object3D();
	camWrapperRotY = new THREE.Object3D();
	
	camWrapperTrans.add( camWrapperRotY );
	camWrapperRotY.add( camWrapperRotX );
	camWrapperRotX.add( camera );
	scene.add( camWrapperTrans );
	
	// The utility functions are worse than useless for the way I've been working for 10 years.
	// Trying something to help out.
	cameraT = new THREE.Object3D();
	cameraT.visible = false;
	camera.add(cameraT);
	
	
	
	// AXIS DISPLAY -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	// var xg = new THREE.SphereGeometry( 0.03, 16, 16 );
	// var xm = new THREE.MeshBasicMaterial( {color: 0xff0000} );
	// var xs = new THREE.Mesh( xg, xm );
	// camWrapperRotX.add( xs );
	
	// var yg = new THREE.SphereGeometry( 0.035, 16, 16 );
	// var ym = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
	// var ys = new THREE.Mesh( yg, ym );
	// camWrapperRotX.add( ys );
	
	// var zg = new THREE.SphereGeometry( 0.04, 16, 16 );
	// var zm = new THREE.MeshBasicMaterial( {color: 0x0000ff} );
	// var zs = new THREE.Mesh( zg, zm );
	// camWrapperTrans.add( zs );
	
	// var blah = new THREE.Mesh( new THREE.SphereGeometry( 0.1, 16, 16 ), xm );
	// blah.translateX(1);
	// scene.add(blah);
	// blah = new THREE.Mesh( new THREE.SphereGeometry( 0.1, 16, 16 ), ym );
	// blah.translateY(1);
	// scene.add(blah);
	// blah = new THREE.Mesh( new THREE.SphereGeometry( 0.1, 16, 16 ), zm );
	// blah.translateZ(1);
	// scene.add(blah);
	
	// var material = new THREE.MeshBasicMaterial( { color: 0xffaa00, wireframe: false, side: THREE.FrontSide  } );
	// var cubeG = new THREE.BoxGeometry( 0.25, 0.25, 0.25 );
	// var mesh = new THREE.Mesh( cubeG, material );
	// scene.add( mesh );
	
	// Alternaively, use the default axes
	 scene.add( new THREE.AxesHelper( 2 ) );
	
	// END AXIS DISPLAY -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	
	
    // SHADER =========================================================================
	// var imagePreviewTexture = new THREE.TextureLoader().load( 'textures/ClusterDot_32.png' );
	// imagePreviewTexture.minFilter = THREE.LinearMipMapLinearFilter;
	// imagePreviewTexture.magFilter = THREE.LinearFilter;
    
	// Slight ISO angle, origin target
	// camera.translateZ(12);
	// camera.translateX(-5);
	// camera.translateY(5);
	// camera.lookAt( new THREE.Vector3(0, 0, 0) );
	
	// Level with origin, reasonable default for debugging single shaders
	camera.translateZ(30);
	camera.translateX(0);
	camera.lookAt( new THREE.Vector3(0, 0, 0) ); // Note that normally, the camera would need to focus on the data center
	
	
	
	// Skydome
	var skyGeo = new THREE.SphereGeometry( 500, 32, 32 );
	// var skyMaterial = new THREE.MeshBasicMaterial( {color: 0xffff00, side: THREE.DoubleSide} );
	var skydomeNoiseTexture = new THREE.TextureLoader().load( 'textures/grey_noise_512.jpg' );
	skydomeNoiseTexture.minFilter = THREE.LinearMipMapLinearFilter;
	skydomeNoiseTexture.magFilter = THREE.LinearFilter;
	skydomeNoiseTexture.wrapS = THREE.RepeatWrapping;
	skydomeNoiseTexture.wrapT = THREE.RepeatWrapping;
	var skyMaterial = new THREE.ShaderMaterial( {
		vertexShader: document.getElementById( 'vshader_skydome' ).textContent,
		fragmentShader: document.getElementById( 'fshader_skydome' ).textContent,
		uniforms: {
    		_MainTex: { type: "t", value: skydomeNoiseTexture }
		},
		side: THREE.DoubleSide
	} );
	var skydome = new THREE.Mesh( skyGeo, skyMaterial );
	scene.add(skydome);
	
	
	window.addEventListener( 'resize', onWindowResize, false );
    
    raycaster = new THREE.Raycaster();
	raycaster.params.Points.threshold = INTERACTIVE_POINT_SIZE; // Varying this is the key to getting good interaction, but 2.5 is a good value for a 5000.0 unit point size? Zoiks.
	
	
	// Based on the shader used in https://threejs.org/examples/?q=instan#webgl_buffergeometry_instancing_billboards
	pointShaderMaterial = new THREE.ShaderMaterial( {
		uniforms: {
			camWorldMatrix: { type: "m4", value: transpose3x3Matrix4(camera.matrixWorldInverse) }
		},
		vertexShader: document.getElementById( 'vshader_datapoint' ).textContent,
		fragmentShader: document.getElementById( 'fshader_datapoint' ).textContent,
		//alphatest: 0.9
		//depthTest: true,
		//depthWrite: true
		// transparent: true,
		// blending: 'NormalBlending',
		// blendSrc: 'SrcAlphaSaturateFactor',
		// blendDst: 'OneMinusSrcAlphaFactor',
		// blendEquation: 'AddEquation'
	} );
}


function onWindowResize()
{
	windowDivW = windowDiv.clientWidth;
	windowDivH = windowDiv.clientHeight;
	windowHalfX = windowDivW / 2;
	windowHalfY = windowDivH / 2;
	
	camera.aspect = windowDivW / windowDivH;
	camera.updateProjectionMatrix();
	
	renderer.setSize( windowDivW, windowDivH );
}


function scaleVector3(v, m)
{
	v.x *= m;
	v.y *= m;
	v.z *= m;
}


function showProps(obj, objName)
{
	var result = "";
	for (var i in obj) {
		if (obj.hasOwnProperty(i)) {
			result += objName + "." + i + " = " + obj[i] + "\n";
		}
	}
	return result;
}




function drawDataObjects()
{
    if (data_GO != null)
    {
        scene.remove(data_GO);
    }
    
    var dataCount = data_objects.length;
    data_mesh_positions = new Float32Array( dataCount * 3 );
	data_mesh_sizes = new Float32Array( dataCount );
	data_mesh_sh1 = new Float32Array( dataCount * 3 );
	data_mesh_sh2 = new Float32Array( dataCount * 3 );
	data_mesh_sh3 = new Float32Array( dataCount * 3 );
	
	var minDist = 1.25; // Scale is somewhat weird now
	var maxDist = 8.0;
	
	for ( var i = 0; i < dataCount; i++ )
    {
		var d = data_objects[i];
        data_mesh_positions[ i * 3 + 0 ] = d['lon'];
		data_mesh_positions[ i * 3 + 1 ] = 0.0;
		data_mesh_positions[ i * 3 + 2 ] = d['lat'];
		
		data_mesh_sizes[i] = DEFAULT_POINT_SIZE;
		
		// Initialize SH9
		for (var j=0; j<3; j++)
		{
			data_mesh_sh1[ (i * 3) + j ] = 0.0;
			data_mesh_sh2[ (i * 3) + j ] = 0.0;
			data_mesh_sh3[ (i * 3) + j ] = 0.0;
		}
		
		// Solve SH9
		for ( var j = 0; j < dataCount; j++ )
    	{
			if ( j != i )
			{
				var n = data_objects[j];
				
				// The idea here is to evaluate every occluder as if it were a directional light, and then use the result to reduce rather than add to the ambient term
				var dv = new THREE.Vector3( n['lon'] - d['lon'], 0.0, n['lat'] - d['lat'] );

				// Solve the distance term for AO, which ideally would consider sphere radius
				var dist = clamp( dv.length(), minDist, maxDist );
				var ao = 1.0 - ( ( dist - minDist ) / (maxDist-minDist) ); // 0..1
				//var ao = ( ( dist - minDist ) / (maxDist-minDist) ); // 0..1
				
				console.log(i + " > " + j);
				console.log(ao);
				
				// Formerly the Ghetto Version
				// This whole thing was originally prototyped in Unity, which has SH9 helper functions, but which didn't work as well as the custom stuff used here and in the shader
				AddLightToSHCapped( data_mesh_sh1, data_mesh_sh2, data_mesh_sh3, i*3, ao, dv.normalize() );
			}
		}
	}
    
	data_mesh = new THREE.BufferGeometry();
	data_mesh.addAttribute( 'position', new THREE.BufferAttribute( data_mesh_positions, 3 ) );
	data_mesh.addAttribute( 'size', new THREE.BufferAttribute( data_mesh_sizes, 1 ) );
	data_mesh.addAttribute( 'attr_sh1', new THREE.BufferAttribute( data_mesh_sh1, 3 ) );
	data_mesh.addAttribute( 'attr_sh2', new THREE.BufferAttribute( data_mesh_sh2, 3 ) );
	data_mesh.addAttribute( 'attr_sh3', new THREE.BufferAttribute( data_mesh_sh3, 3 ) );

	data_GO = new THREE.Points( data_mesh, pointShaderMaterial );
    data_GO.name = "data_GO";
	
    scene.add( data_GO );
}

// SH consts from https://en.wikipedia.org/wiki/Table_of_spherical_harmonics
// There are listed in the weird order from https://docs.google.com/presentation/d/1bSw3RQLPkXkzVnlpnt4F-Tj0JQS5N6JgANSPrpZNLIA/edit#slide=id.p27 
var shC1 = 0.5 * Math.sqrt(1/Math.PI);
var shC2 = Math.sqrt(0.75 * Math.PI);
var shC3 = 0.5 * (Math.sqrt(15/Math.PI));
var shC4 = 0.25 * (Math.sqrt(5/Math.PI));
var shC5 = 0.25 * (Math.sqrt(15/Math.PI));

// Got this from https://docs.google.com/presentation/d/1bSw3RQLPkXkzVnlpnt4F-Tj0JQS5N6JgANSPrpZNLIA/edit#slide=id.p27
function AddLightToSHCapped ( sh1, sh2, sh3, o, amt, dirn )
{
	var a0 = amt * shC1;

	var a1 = amt * shC2 * dirn.x;
	var a2 = amt * shC2 * dirn.y;
	var a3 = amt * shC2 * dirn.z;

	var a4 = amt * shC3 * ( dirn.x * dirn.z );
	var a5 = amt * shC3 * ( dirn.z * dirn.y );
	var a6 = amt * shC3 * ( dirn.y * dirn.x );
	var a7 = amt * shC4 * ( 3.0 * dirn.z * dirn.z - 1.0 ); // It's amazing to me that this is also ( -(dirn.x*dirn.x) - (dirn.y*dirn.y) + (2.0*dirn.z*dirn.z) ), by xx + yy + zz = 1
	var a8 = amt * shC5 * ( dirn.x * dirn.x - dirn.y * dirn.y );

	var d = Math.abs(a0-sh1[o+0]) + Math.abs(a1-sh1[o+1]) + Math.abs(a2-sh1[o+2]) + Math.abs(a3-sh2[o+0]) + Math.abs(a4-sh2[o+1]) + Math.abs(a5-sh2[o+2]) + Math.abs(a6-sh3[o+0]) + Math.abs(a7-sh3[o+1]) + Math.abs(a8-sh3[o+2]);

	d = clamp01( d / 3.0 );

	sh1[ o + 0 ] += a0 * d;
	sh1[ o + 1 ] += a1 * d;
	sh1[ o + 2 ] += a2 * d;
	sh2[ o + 0 ] += a3 * d;
	sh2[ o + 1 ] += a4 * d;
	sh2[ o + 2 ] += a5 * d;
	sh3[ o + 0 ] += a6 * d;
	sh3[ o + 1 ] += a7 * d;
	sh3[ o + 2 ] += a8 * d;
}


















var refMouseX;
var refMouseY;
var lastMouseX;
var lastMouseY;
var refCamRotationX;
var refCamRotationY;
var refCamDist; // Distance from camera to center of wrapper chain.
var refCamPosition; // Vector3 pos of transwrapper.
var KEY_SHIFT = 16;
var KEY_CTRL = 17;
var KEY_ALT = 18;
var MOUSE_LMB = 257;
var isMouseReady = false;
var keyDownMap = {
	KEY_SHIFT : false, // Shift
	KEY_CTRL : false, // Control
	KEY_ALT : false, // Alt
	MOUSE_LMB : false
};

var V_RIGHT = new THREE.Vector3(  1,  0,  0 );
var V_LEFT  = new THREE.Vector3( -1,  0,  0 );
var V_UP    = new THREE.Vector3(  0,  1,  0 );
var V_DOWN  = new THREE.Vector3(  0, -1,  0 );
var V_FORE  = new THREE.Vector3(  0,  0,  1 );
var V_BACK  = new THREE.Vector3(  0,  0, -1 );

function onDocumentMouseMove( e )
{
	e.preventDefault();
	
	// Some comments on clientXY vs pageXY vs screenXY
	// https://stackoverflow.com/questions/6073505/what-is-the-difference-between-screenx-y-clientx-y-and-pagex-y
	// As all of these systems are window or monitor relative and not element relative, you have to handle offsets yourself.
	// In the case(s?) that matter here, this is thankfully very simple, as only the 3D window should care, and it's just one div
	//   with a little scoot in on the right.
	var eX = event.clientX - windowDivOffsetX;
	var eY = event.clientY;
	
	mouseX = eX - windowHalfX;
	mouseY = eY - windowHalfY;
	
	if (!isMouseReady)
	{
		isMouseReady = true;
		lastMouseX = mouseX;
		lastMouseY = mouseY;
	}
    
    rayMouse.x = ( eX / windowDivW ) * 2 - 1;
	rayMouse.y = - ( eY / windowDivH ) * 2 + 1;
	
	
	// var geometry = data_GO.geometry;
	// var attributes = geometry.attributes;
	// attributes.size.needsUpdate = true;
	raycaster.setFromCamera( rayMouse, camera );
    var intersects = raycaster.intersectObject( data_GO );
	if (intersects.length > 0)
		console.log(intersects.length);
}
function onDocumentKeyDown( e )
{
	keyDownMap[ e.keyCode ] = true;
	
	// Alt to orbit
	if ( e.keyCode == KEY_ALT )
	{
		refMouseX = mouseX;
		refMouseY = mouseY;
		refCamRotationX = camWrapperRotX.rotation.x;
		refCamRotationY = camWrapperRotY.rotation.y;
	}
	
	// Alt+Shift to pan
	if ( e.keyCode == KEY_ALT && keyDownMap[KEY_SHIFT] == true )
	{
		refCamPosition = camWrapperTrans.position;
	}
	
	// Alt+Control to zoom
	if ( e.keyCode == KEY_ALT && keyDownMap[KEY_CTRL] == true )
	{
		refCamDist = camera.position.z;
	}
}
function onDocumentKeyUp( e )
{
	keyDownMap[ e.keyCode ] = false;
}
function onDocumentMouseDown( e )
{
	keyDownMap[ MOUSE_LMB ] = true;
	// Raycast to select focus
    raycaster.setFromCamera( rayMouse, camera );
    var intersects = raycaster.intersectObject( data_GO );
    if ( intersects.length > 0 ) {
        
        // This sucks!
        // Massive accuracy problems if any two points are even close to overlapping,
        //   which you know, in a cloud of 10K points is basically a given.
        // Taken from:
        //   http://threejs.org/examples/#webgl_interactive_points
        //   http://threejs.org/examples/webgl_interactive_points.html
        //   where it seems to be pretty stable, not sure what the difference is.
        // var i = intersects[0].index;
        // MESSENGER.send(data_objects[i], "focus")
		
        // Trying a loop where we select the intersection with the smallest distance
        //   between world intersection point and indexed point position.
		// UPDATE: Still pretty much horrible, needs more investigation. Maybe InstanceBuffer of a quad is better than going for points? Need to do billboarding logic but that's it.
        var dist = 99999999;
        var index = 0;
		
        for (var i=0; i<intersects.length; i++)
        {
            v = new THREE.Vector3(
                data_mesh_positions[intersects[i].index + 0],
                data_mesh_positions[intersects[i].index + 1],
                data_mesh_positions[intersects[i].index + 2]
            );
            var p = intersects[i].point;
            var d = p.sub(v).length();
            
            console.log("  ", i, intersects[i].index, p, intersects[i]);
        }
		index = intersects[0].index;
        MESSENGER.send(data_objects[index], "focus")
    }
}
function onDocumentMouseUp( e )
{
	keyDownMap[ MOUSE_LMB ] = false;
}







function onRenderScene()
{
	// Process data change events
	while (eventQueue.events.length > 0)
	{
		var e = eventQueue.events.shift();
		switch(e.type)
		{
			case "data":
				console.log("[Explorer] Data event being handled...");
                data_objects = e.body;
				drawDataObjects();
				break;
		}
	}
	
	// CTRL must be held for any navigation features.
	if ( keyDownMap[ KEY_CTRL ] == true )
	{
		if ( keyDownMap[ MOUSE_LMB ] == true ) // Must be click/holding for nav
		{
			var ldx = mouseX - lastMouseX;
			var ldy = mouseY - lastMouseY;
			
			if ( keyDownMap[ KEY_SHIFT ] == true ) // Shift to pan
			{
				// magnitude(CamLocalPos)*c is a zoom-based heuristic to determine movement amount.
				// If FoV varied indepedently, it would be considered here as well.
				// The constant is a magic number and probably wrong at some scales.
				var c = camera.position.length() * 1.1;
				
				// Get the X/Y plane movement in camera local space and convert to world...
				var v = new THREE.Vector3(-ldx/c, ldy/c, 0);
				cameraT.localToWorld(v);
				v.sub(cameraT.getWorldPosition());
				
				// And apply it to the translate wrapper in world space
				camWrapperTrans.translateOnAxis(v, 1);
			}
			else // Just CTRL to orbit
			{
				camWrapperRotY.rotateY(-ldx/425);
				camWrapperRotX.rotateX(-ldy/425);
			}
		}
	}
    
	//pointShaderMaterial.uniforms.camWorldMatrix.value = camera.matrixWorldInverse.transpose(); // Doesn't quite do it, using custom function instead.
	pointShaderMaterial.uniforms.camWorldMatrix.value = transpose3x3Matrix4(camera.matrixWorldInverse);
	
	
	
	
	// Example of attribute update
	// NOTE that only axis definition changes would ever do frame-over-frame animations
	// The SH9 updates would be done some other way, presumably during initial creation... actually, can it just be solved during build? Maybe right?
	// -- EXAMPLE ONLY !!!  --  taken from : https://threejs.org/examples/#webgl_buffergeometry_custom_attributes_particles
	// var sizes = geometry.attributes.size.array;
	// for ( var i = 0; i < particles; i++ ) {
	// 	sizes[ i ] = 10 * ( 1 + Math.sin( 0.1 * i + time ) );
	// }
	// geometry.attributes.size.needsUpdate = true;
	
	
	
	
	renderer.clear();
	renderer.clearDepth();
	renderer.render( scene, camera );
	requestAnimationFrame( onRenderScene );
	
	if (isMouseReady)
	{
		lastMouseX = mouseX;
		lastMouseY = mouseY;
	}
}


init();
onRenderScene();