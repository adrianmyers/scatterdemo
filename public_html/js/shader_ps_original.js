uniform sampler2D tex1;
varying float vAlpha;

void main() {
	//gl_FragColor = texture2D(tex1, gl_PointCoord);
	vec4 textureColor = texture2D(tex1, gl_PointCoord);
	gl_FragColor = vec4(textureColor.rgb, textureColor.a);
	if ( gl_FragColor.a < ALPHATEST ) discard;
}