﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor( typeof( SphereControl ) )]
public class SphereControlInspector : Editor
{

	public override void OnInspectorGUI ()
	{
		SphereControl myTarget = (SphereControl)target;

		if ( GUILayout.Button( "Solve Sphere AO (Ambient Cube)" ) )
		{
			SolveSphereAOAmbientCube( myTarget );
		}

		if ( GUILayout.Button( "Solve Sphere AO (SH)" ) )
		{
			SolveSphereAOSH( myTarget );
		}
	}



	private void SolveSphereAOAmbientCube ( SphereControl myTarget )
	{
		Transform xt = myTarget.gameObject.transform;

		GameObject go = null;
		MeshFilter mf = null;
		Mesh mesh = null;
		Color[] cols = null;
		Vector3[] cube1 = null;
		Vector3[] cube2 = null;
		Color c = Color.white;

		float minDist = 2f;
		float maxDist = 10f;

		foreach ( Transform t in xt )
		{
			go = t.gameObject;
			mf = go.GetComponent<MeshFilter>();
			mesh = mf.sharedMesh;
			cube1 = new Vector3[ mesh.vertices.Length ];
			cube2 = new Vector3[ mesh.vertices.Length ];
			cols = new Color[ mesh.vertices.Length ];

			//Debug.Log( "Processing " + t.name + " : " + mesh.GetInstanceID() );

			// Zero out AO coeffs
			for ( int i = 0; i < cube1.Length; i++ )
			{
				cube1[ i ].x = 0f; // +X
				cube1[ i ].y = 0f; // +Y
				cube1[ i ].z = 0f; // +Z

				cube2[ i ].x = 0f; // -X
				cube2[ i ].y = 0f; // -Y
				cube2[ i ].z = 0f; // -Z

				cols[ i ] = t.name == "s01" ? new Color( 1f, 0.45f, 0.0f ) : Color.white;
			}

			foreach ( Transform o in xt )
			{
				if ( o != t && o.gameObject.activeSelf )
				{
					// Solve the distance term for AO, which ideally would consider sphere radius
					float dist = Mathf.Clamp( (o.position - t.position).magnitude, minDist, maxDist );
					float ao = 1f - ( ( dist - minDist ) / (maxDist-minDist) ); // 0..1

					// Solve the angle/dot() term
					// We don't really need to do 6 dots, since the sign and value of the normalized delta vector tell us which coeffs to modify and by how much
					Vector3 dv = (o.position - t.position).normalized;

					// So in WebGL, we use mesh data because each sphere is really a point in a cloud mesh.
					// Here, we are really setting a per-object global, but I don't want to do the material spam thing, at least partly because I don't want this code
					//   to be completely useless when porting to WebGL, so I'm trying to keep conceptual and method divergence to a minimum.
					// We can do that by writing a whole mesh buffer with one value, so that e.g. Normals/Cube1.x is constant throughout the whole mesh, and acts like the 
					//   value would for the single vertex being passed in WebGL's case.
					Vector3[] cube = dv.x > 0f ? cube1 : cube2;
					for ( int i = 0; i < cube.Length; i++ )
						cube[ i ].x = Mathf.Clamp01( cube[ i ].x + Mathf.Abs( ao * dv.x ) );

					cube = dv.y > 0f ? cube1 : cube2;
					for ( int i = 0; i < cube.Length; i++ )
						cube[ i ].y = Mathf.Clamp01( cube[ i ].y + Mathf.Abs( ao * dv.y ) );

					cube = dv.z > 0f ? cube1 : cube2;
					for ( int i = 0; i < cube.Length; i++ )
						cube[ i ].z = Mathf.Clamp01( cube[ i ].z + Mathf.Abs( ao * dv.z ) );

					// Alternate version, just debugging to see if a more naive approach does anything differently/better, not loving the weird gaps this is creating.
					//for ( int i = 0; i < cube1.Length; i++ )
					//{

					//}


				}
			}
			mesh.SetNormals( new List<Vector3>( cube1 ) );
			mesh.SetUVs( 1, new List<Vector3>( cube2 ) );
			mesh.SetColors( new List<Color>( cols ) );
		}
	}

	private void SolveSphereAOSH ( SphereControl myTarget )
	{
		Transform xt = myTarget.gameObject.transform;

		GameObject go = null;
		MeshFilter mf = null;
		Mesh mesh = null;
		Color[] cols = null;
		Vector3[] sh1 = null;
		Vector3[] sh2 = null;
		Vector3[] sh3 = null;
		Color c = Color.white;

		float minDist = 1f;
		float maxDist = 10f;

		foreach ( Transform t in xt )
		{
			go = t.gameObject;
			mf = go.GetComponent<MeshFilter>();
			mesh = mf.sharedMesh;
			cols = new Color[ mesh.vertices.Length ];
			sh1 = new Vector3[ mesh.vertices.Length ];
			sh2 = new Vector3[ mesh.vertices.Length ];
			sh3 = new Vector3[ mesh.vertices.Length ];

			//Debug.Log( "Processing " + t.name + " : " + mesh.GetInstanceID() );

			// Zero out AO coeffs
			for ( int i = 0; i < sh1.Length; i++ )
			{
				sh1[ i ].x = 0f;
				sh1[ i ].y = 0f;
				sh1[ i ].z = 0f;

				sh2[ i ].x = 0f;
				sh2[ i ].y = 0f;
				sh2[ i ].z = 0f;

				sh3[ i ].x = 0f;
				sh3[ i ].y = 0f;
				sh3[ i ].z = 0f;

				cols[ i ] = t.name == "s01" ? new Color( 1f, 0.45f, 0.0f ) : Color.white;
			}

			// Ghetto version, but since I only have shader code for the ghetto version, it's what's for dinner
			float[] sh9 = new float[9] { 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f };

			foreach ( Transform o in xt )
			{
				if ( o != t && o.gameObject.activeSelf )
				{
					// The idea here is to evaluate every occluder as if it were a directional light, and then use the result to reduce rather than add to the ambient term
					Vector3 dv = (o.position - t.position);

					// Solve the distance term for AO, which ideally would consider sphere radius
					float dist = Mathf.Clamp( dv.magnitude, minDist, maxDist );
					float ao = 1f - ( ( dist - minDist ) / (maxDist-minDist) ); // 0..1

					// Unity version
					//sh.AddDirectionalLight( dv, Color.white, ao );

					// Ghetto version
					//AddLightToSH( sh9, ao, dv.normalized );
					AddLightToSHCapped( sh9, ao, dv.normalized );
				}
			}

			// Ghetto version
			for ( int i = 0; i < sh1.Length; i++ )
			{
				sh1[ i ].x = sh9[ 0 ];

				sh1[ i ].y = sh9[ 1 ];
				sh1[ i ].z = sh9[ 2 ];
				sh2[ i ].x = sh9[ 3 ];

				sh2[ i ].y = sh9[ 4 ];
				sh2[ i ].z = sh9[ 5 ];
				sh3[ i ].x = sh9[ 6 ];
				sh3[ i ].y = sh9[ 7 ];
				sh3[ i ].z = sh9[ 8 ];
			}

			mesh.SetUVs( 0, new List<Vector3>( sh1 ) );
			mesh.SetUVs( 1, new List<Vector3>( sh2 ) );
			mesh.SetUVs( 2, new List<Vector3>( sh3 ) );
			mesh.SetColors( new List<Color>( cols ) );
		}
	}

	// Got this from https://docs.google.com/presentation/d/1bSw3RQLPkXkzVnlpnt4F-Tj0JQS5N6JgANSPrpZNLIA/edit#slide=id.p27
	// Then realized I have no idea what fConst1-5 are, or why there are 3 different variables in the third band.
	void AddLightToSH ( float[] sh9, float amt, Vector3 dirn )
	{
		// ¯\_(ツ)_/¯
		//float fConst1 = 1f;
		//float fConst2 = 1f;
		//float fConst3 = 1f;
		//float fConst4 = 1f;
		//float fConst5 = 1f;

		float fConst1 = 1f;
		float fConst2 = 0.8f;
		float fConst3 = 0.5f;
		float fConst4 = 0.5f;
		float fConst5 = 0.5f;


		//float fConst1 = 0.5f;
		//float fConst2 = 0.8f;
		//float fConst3 = 1f;
		//float fConst4 = 1f;
		//float fConst5 = 1f;

		sh9[ 0 ] += amt * fConst1;

		sh9[ 1 ] += amt * fConst2 * dirn.x;
		sh9[ 2 ] += amt * fConst2 * dirn.y;
		sh9[ 3 ] += amt * fConst2 * dirn.z;

		sh9[ 4 ] += amt * fConst3 * ( dirn.x * dirn.z );
		sh9[ 5 ] += amt * fConst3 * ( dirn.z * dirn.y );
		sh9[ 6 ] += amt * fConst3 * ( dirn.y * dirn.x );
		sh9[ 7 ] += amt * fConst4 * ( 3.0f * dirn.z * dirn.z - 1.0f );
		sh9[ 8 ] += amt * fConst5 * ( dirn.x * dirn.x - dirn.y * dirn.y );

		// SHOOR!
		//for ( int i = 0; i < 9; i++ )
		//	sh9[ i ] = Mathf.Min( sh9[ i ], 0.5f );
		sh9[ 0 ] = Mathf.Min( sh9[ 0 ], fConst1 );
		sh9[ 1 ] = Mathf.Min( sh9[ 1 ], fConst2 );
		sh9[ 2 ] = Mathf.Min( sh9[ 2 ], fConst2 );
		sh9[ 3 ] = Mathf.Min( sh9[ 3 ], fConst2 );
		sh9[ 4 ] = Mathf.Min( sh9[ 4 ], fConst3 );
		sh9[ 5 ] = Mathf.Min( sh9[ 5 ], fConst3 );
		sh9[ 6 ] = Mathf.Min( sh9[ 6 ], fConst3 );
		sh9[ 7 ] = Mathf.Min( sh9[ 7 ], fConst4 );
		sh9[ 8 ] = Mathf.Min( sh9[ 8 ], fConst5 );
	}


	void AddLightToSHCapped ( float[] sh9, float amt, Vector3 dirn )
	{
		// No idea what these are supposed to do or be, but they look allright
		float fConst1 = 1f;
		float fConst2 = 0.8f;
		float fConst3 = 0.5f;
		float fConst4 = 0.5f;
		float fConst5 = 0.5f;

		float a0 = amt * fConst1;

		float a1 = amt * fConst2 * dirn.x;
		float a2 = amt * fConst2 * dirn.y;
		float a3 = amt * fConst2 * dirn.z;

		float a4 = amt * fConst3 * ( dirn.x * dirn.z );
		float a5 = amt * fConst3 * ( dirn.z * dirn.y );
		float a6 = amt * fConst3 * ( dirn.y * dirn.x );
		float a7 = amt * fConst4 * ( 3.0f * dirn.z * dirn.z - 1.0f );
		float a8 = amt * fConst5 * ( dirn.x * dirn.x - dirn.y * dirn.y );

		float d = Mathf.Abs(a0-sh9[0]) + Mathf.Abs(a1-sh9[1]) + Mathf.Abs(a2-sh9[2]) + Mathf.Abs(a3-sh9[3]) + Mathf.Abs(a4-sh9[4]) + Mathf.Abs(a5-sh9[5]) + Mathf.Abs(a6-sh9[6]) + Mathf.Abs(a7-sh9[7]) + Mathf.Abs(a8-sh9[8]);

		d = Mathf.Clamp01( d / 5f );

		sh9[ 0 ] += a0 * d;
		sh9[ 1 ] += a1 * d;
		sh9[ 2 ] += a2 * d;
		sh9[ 3 ] += a3 * d;
		sh9[ 4 ] += a4 * d;
		sh9[ 5 ] += a5 * d;
		sh9[ 6 ] += a6 * d;
		sh9[ 7 ] += a7 * d;
		sh9[ 8 ] += a8 * d;
	}

}