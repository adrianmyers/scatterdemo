﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "AW/SHAO"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color  : COLOR;
				float3 uv     : TEXCOORD0;
				float3 uv2    : TEXCOORD1;
				float3 uv3    : TEXCOORD2;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR;   // -- Debug only, not needed
				float3 sh1   : TEXCOORD0;
				float3 sh2   : TEXCOORD1;
				float3 sh3   : TEXCOORD2;
				float3 pos   : TEXCOORD3; // Acting as normal, which in the billboard has to be solved from UV
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.pos = v.vertex.xyz;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;

				o.sh1 = v.uv;
				o.sh2 = v.uv2;
				o.sh3 = v.uv3;

				return o;
			}
			
			float GetSH(float3 dirn, float3 sh1, float3 sh2, float3 sh3)
			{
				float colour = sh1[0];
				//
				colour += sh1[1] * dirn.x;
				colour += sh1[2] * dirn.y;
				colour += sh2[0] * dirn.z;
				//
				colour += sh2[1] * (dirn.x * dirn.z);
				colour += sh2[2] * (dirn.z * dirn.y);
				colour += sh3[0] * (dirn.y * dirn.x);
				colour += sh3[1] * (3.0f * dirn.z * dirn.z - 1.0f);
				colour += sh3[2] * (dirn.x * dirn.x - dirn.y * dirn.y);
				return colour;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = half4(i.color.rgb, 1); // Vertex colors, although I don't they're not doing much now. In WebGL, they will probably control selection visibility (?)
				
				float3 wn = normalize(i.pos); // It's a unit sphere
				float up = dot(wn, float3(0,1,0));
				up = (up*0.5) + 0.5;
				col.rgb = lerp(half3(0.5, 0.5, 0.5), half3(0.9, 0.92, 0.97), up);
				//col.rgb = half3(0.5, 0.5, 0.5);
				
				// Sunny, off-vertical Lighting to give things a "corner", not sure it matters
				float3 light = normalize(float3(0.2, 1, -0.3));
				float lr = dot(wn, light);
				lr = (lr*0.5) + 0.5; // If you don't do this, you get a "add or subtract" effect based on angle, which is actually kind of nice
				//col.rgb += lerp(half3(0.0, 0.0, 0.0), half3(0.2, 0.15, 0.1), lr);

				float ao = GetSH( i.pos * 2, i.sh1, i.sh2, i.sh3 );

				// Mult
				//col.rgb *= 1-(0.15 * ao);
				col.rgb *= 1 - (0.0375 * ao * ao); // Big fan of this effect

				// Add
				//col.rgb -= 0.09 * ao; // Linear, good default, maybe a bit bright overall
				//col.rgb -= 0.028 * ao * ao; // I like this as well


				// Selection tinting (assuming selection controls vertex color in WebGL)
				col.rgb *= i.color;

				// DAT FRESNEL DOE
				// Note that this is much simpler in the billboard version, as we can just use the UVs. But here we have to do quite a lot of crap.
				// This would be better done in the vertex shader, but since the whole point is to build something that can be ported,
				//   and this won't work the same way, I'm trying not to clutter that shader with more stuff to remove.
				float3 vn = normalize(_WorldSpaceCameraPos.xyz - mul(unity_ObjectToWorld, float4(i.pos, 1)).xyz);
				float fr = pow(1-saturate(dot(wn, vn)), 1.2) * 0.4; // This can be played with a lot... wide and soft for fewer artifacts though, esp in webgl where AA is not really a thing that's happening.
				//col.rgb += fr * half3(0.9, 0.9, 1);
				col.rgb += fr * half3(0.7, 0.8, 1); // I kind of like the seaspray
				
				
				//col.rgb = i.pos.x > 0.45 ? half3(1,1,1) : half3(0, 0, 0);
				//col.rgb = i.pos.x > 0 ? length(normalize(i.pos)) : length(i.pos);

				return col;
			}
			ENDCG
		}
	}
}
