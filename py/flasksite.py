from flask import Flask, request, Response
from flask_cors import CORS
from fakedb import Dbcontrol
from datetime import datetime, date
from dateutil import parser
import psycopg2
import json


app = Flask(__name__)
app.debug = True
CORS(app)

g_dbname = "amtest"  # amtest, scatterdata
g_username = "Adrian"  # Adrian, Adrian.Myers


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    if isinstance(obj, date):
        serial = obj.isoformat()
        return serial
    if isinstance(obj, None):
        serial = obj.isoformat()
        return None
    raise TypeError ("Type not serializable")


@app.route('/')
def hello_world():
    return "Hello World!"


# Example: { "query" : "select * from purchases where item=1 limit 10 offset 1;", "format" : "csv" }
@app.route('/query', methods=['POST'])
def handle_query():
    d = Dbcontrol()

    j = request.get_json()
    qs = j['query']
    format = j['format'] if 'format' in j else 'json'

    o = d.perform_query(qs)

    res = None
    if format == 'csv':
        # CSV, does not handle nested objects
        res = Response(
            status   = 200,
            mimetype = "text/plain",
            response = list_to_csv(o)
        )
    else:
        # JSON, default
        res = Response(
            status   = 200,
            mimetype = "application/json",
            response = json.dumps(o)
        )

    return res


@app.route('/testpeople', methods=['GET'])
def build_people():
    conn = psycopg2.connect("dbname=" + g_dbname + " user=" + g_username)
    cur = conn.cursor()

    # Get the items!
    items = []
    sql = "SELECT id, name, price, category FROM items"
    cur.execute(sql)
    results = cur.fetchall()
    for r in results:
        item = {
            'id' : r[0],
            'name' : r[1],
            'price' : r[2],
            'category' : r[3]
        }
        items.append(item)


    # Get the people!
    persons = []
    sql = "SELECT id, firstname, lastname, age, location FROM persons"
    cur.execute(sql)
    results = cur.fetchall()
    for r in results:
        pos = str(r[4])
        lat = float(pos.split(',')[0][1:])
        lon = float(pos.split(',')[1][0:-1])
        person = {
            'id' : r[0],
            'firstname' : r[1],
            'lastname' : r[2],
            'age' : r[3],
            'location' : r[4],
            'lat' : lat,
            'lon' : lon,
            'purchases' : []
        }

        # Associate this person with some purchases of items!
        sql = "SELECT id, person, item, cost, time, location FROM purchases WHERE person=" + str(person['id'])
        cur.execute(sql)
        p_results = cur.fetchall()
        for p in p_results:
            moo = 1
            purchase = {
                'id' : p[0],
                'purchaserID' : p[1],
                'itemID' : p[2],
                'cost' : p[3],
                'time' : p[4].isoformat(),
                'location' : str(p[5])
            }
            person['purchases'].append(purchase)
        persons.append(person)

    s = json.dumps(persons, default=json_serial)
    res = Response(
        status   = 200,
        mimetype = "application/json",
        response = s
    )
    return res




# Assumes l is list of flat objects
def list_to_csv(l):
    s = ""
    # l is a list of Python objects in a JSON-like format.
    # We need to convert to CSV where the first line is the column names, and note that these will be in a completely arbitrary order each time.
    # Get the first element, and iterate over it to retrieve keys, which are the column names. Write these out first.
    for k in l[0]:
        s += k+","
    s = s[:-1] + '\n'

    # The iterate over all items, writing out the values only.
    for o in l:
        for k in o:
            v = o[k]
            s += '"'+str(v)+'",'
        s = s[:-1] + '\n'

    return s


if __name__ == '__main__':
    app.run()
