import psycopg2
import json
import os
from datetime import datetime
import random


class Dbcontrol:

    def __init__(self):
        self.dbname = "scatterdata"  # amtest, scatterdata
        self.username = "Adrian.Myers"  # Adrian, Adrian.Myers

    def create_table_persons(self):
        conn = psycopg2.connect("dbname=" + self.dbname + " user=" + self.username)
        cur = conn.cursor()

        try:
            cur.execute("CREATE TABLE persons (id serial PRIMARY KEY, firstname varchar, lastname varchar, age integer, location point);")
            conn.commit()
        except ValueError as e:
            print("** ERROR creating table")
            print(e)
            cur.close()
            conn.close()

        cur.close()
        conn.close()


    def create_persons(self):
        conn = psycopg2.connect("dbname=" + self.dbname + " user=" + self.username)
        cur = conn.cursor()

        cur.execute("insert into persons (firstname, lastname, age, location) values('Bob', 'Smith', 50, '(10,10)');")
        cur.execute("insert into persons (firstname, lastname, age, location) values('Bill', 'Buckwaller', 34, '(10,20)');")
        cur.execute("insert into persons (firstname, lastname, age, location) values('Joe', 'Homes', 41, '(16,13)');")
        cur.execute("insert into persons (firstname, lastname, age, location) values('Isabella', 'Krutzmunster', 23, '(10,16)');")
        cur.execute("insert into persons (firstname, lastname, age, location) values('Jordan', 'Daschel', 45, '(13,12)');")
        cur.execute("insert into persons (firstname, lastname, age, location) values('Mustafa', 'White', 27, '(15,21)');")
        cur.execute("insert into persons (firstname, lastname, age, location) values('Ahmed', 'Guillerme', 40, '(14,11)');")
        cur.execute("insert into persons (firstname, lastname, age, location) values('Hazel', 'Nuts', 32, '(19,19)');")
        cur.execute("insert into persons (firstname, lastname, age, location) values('Salamander', 'jrpg', 15, '(16,10)');")
        cur.execute("insert into persons (firstname, lastname, age, location) values('Nubuo', 'Iwata', 52, '(10,17)');")

        try:
            conn.commit()
        except ValueError as e:
            print("** ERROR creating table")
            print(e)
            conn.rollback()
            cur.close()
            conn.close()




    def create_table_items(self):
        conn = psycopg2.connect("dbname=" + self.dbname + " user=" + self.username)
        cur = conn.cursor()

        try:
            cur.execute("CREATE TABLE items (id serial PRIMARY KEY, name varchar, price float4, category int);")
            conn.commit()
        except ValueError as e:
            print("** ERROR creating table")
            print(e)
            cur.close()
            conn.close()

        cur.close()
        conn.close()



    def create_items(self):
        conn = psycopg2.connect("dbname=" + self.dbname + " user=" + self.username)
        cur = conn.cursor()

        cur.execute("insert into items (name, price, category) values ('Buttscratcha!', 4.5, 0);")
        cur.execute("insert into items (name, price, category) values ('Midori MD Notebook', 10.0, 0);")
        cur.execute("insert into items (name, price, category) values ('Pens', 6.0, 0);")

        cur.execute("insert into items (name, price, category) values ('Salad', 9.5, 1);")
        cur.execute("insert into items (name, price, category) values ('Burger', 4.5, 1);")
        cur.execute("insert into items (name, price, category) values ('Pizza', 8.0, 1);")
        cur.execute("insert into items (name, price, category) values ('Noodles', 7.0, 1);")

        cur.execute("insert into items (name, price, category) values ('Tennis Racquet', 50, 2);")
        cur.execute("insert into items (name, price, category) values ('Running Shoes', 100, 2);")

        cur.execute("insert into items (name, price, category) values ('TV', 800.0, 3);")
        cur.execute("insert into items (name, price, category) values ('Console', 400.0, 3);")

        try:
            conn.commit()
        except ValueError as e:
            print("** ERROR creating table")
            print(e)
            conn.rollback()
            cur.close()
            conn.close()





    def create_table_purchases(self):
        conn = psycopg2.connect("dbname=" + self.dbname + " user=" + self.username)
        cur = conn.cursor()

        try:
            cur.execute("CREATE TABLE purchases (id serial PRIMARY KEY, person integer references persons(id), item integer references items(id), cost float4, time timestamp, location point, lat float8, lon float8);")
            conn.commit()
        except ValueError as e:
            print("** ERROR creating table")
            print(e)
            cur.close()
            conn.close()

        cur.close()
        conn.close()


    def create_purchases(self):
        conn = psycopg2.connect("dbname=" + self.dbname + " user=" + self.username)
        cur = conn.cursor()

        persons = [] # id, namef, namel, age, loc
        items = [] # id, name, cost, category
        try:
            cur.execute("select * from persons;")
            for r in cur:
                persons.append(r)
            conn.commit()

            cur.execute("select * from items;")
            for r in cur:
                items.append(r)
            conn.commit()
        except ValueError as e:
            print("** ERROR retrieving persons")
            print(e)
            cur.close()
            conn.close()
            exit()


        cats = {}
        for item in items:
            cat = str(item[3])
            if not cat in cats:
                cats[cat] = []
            cats[cat].append(item)


        # Let's create two months worth of data
        # Basically we'll have 60 days, with each person having a 3 high chances at buying a cat1 meal, a high chance of a cat0 item, and lower chances of cat2 and cat3 items.
        for i in range(1,60):
            day_str = ""
            if i<31:
                day_str = "2015-06-" + str(i)
            else:
                day_str = "2015-07-" + str(i-30)

            for person in persons:

                # Three meals at distinct times with very high probability
                for j in range(0,2):
                    if random.random() < 0.9:
                        if j==0:
                            time_str = day_str + "T08:"+str(random.randint(0,59))
                        elif j==1:
                            time_str = day_str + "T12:"+str(random.randint(0,59))
                        else:
                            time_str = day_str + "T18:"+str(random.randint(0,59))
                        item = cats['1'][random.randint(0,len(cats['1'])-1)]
                        pos = str(person[4])
                        lat = pos.split(',')[0][1:]
                        lon = pos.split(',')[1][0:-1]
                        qs = "insert into purchases (person, item, cost, time, location, lat, lon) values ("+ str(person[0]) +", "+ str(item[0]) +", "+ str(item[2]) +", '"+ time_str +"', '"+ pos +"', '"+ lat +"', '"+ lon +"')"
                        cur.execute(qs)

                # Two chances to buy small items with high probabiliy
                for j in range(0,1):
                    if random.random() < 0.7:
                        time_str = day_str + "T"+str(random.randint(0,23))+":"+str(random.randint(0,59))
                        item = cats['0'][random.randint(0,len(cats['0'])-1)]
                        qs = "insert into purchases (person, item, cost, time, location, lat, lon) values (" + str(person[0]) + ", " + str(item[0]) + ", " + str(item[2]) + ", '" + time_str + "', '" + pos + "', '" + lat + "', '" + lon + "')"
                        cur.execute(qs)


                # Low chance of bigger item
                if random.random() < 0.3:
                    time_str = day_str + "T"+str(random.randint(0,23))+":"+str(random.randint(0,59))
                    item = cats['2'][random.randint(0,len(cats['2'])-1)]
                    qs = "insert into purchases (person, item, cost, time, location, lat, lon) values (" + str(person[0]) + ", " + str(item[0]) + ", " + str(item[2]) + ", '" + time_str + "', '" + pos + "', '" + lat + "', '" + lon + "')"
                    cur.execute(qs)

                # Very low chance of biggest item
                if random.random() < 0.05:
                    time_str = day_str + "T"+str(random.randint(0,23))+":"+str(random.randint(0,59))
                    item = cats['3'][random.randint(0,len(cats['3'])-1)]
                    qs = "insert into purchases (person, item, cost, time, location, lat, lon) values (" + str(person[0]) + ", " + str(item[0]) + ", " + str(item[2]) + ", '" + time_str + "', '" + pos + "', '" + lat + "', '" + lon + "')"
                    cur.execute(qs)

        try:
            conn.commit()
        except ValueError as e:
            print("** ERROR creating purchases")
            print(e)
            cur.close()
            conn.close()
            exit()

        cur.close()
        conn.close()


    def modify_purchases(self):
        conn = psycopg2.connect("dbname=" + self.dbname + " user=" + self.username)
        cur = conn.cursor()

        try:
            cur.execute("select * from purchases;")
            purchases = cur.fetchall()
            for purchase in purchases:
                id = purchase[0]
                p = purchase[5]
                lat = p.split(',')[0][1:]
                lon = p.split(',')[1][0:-1]
                qs = "update purchases set (lat, lon) = ("+ lat +", "+ lon +") where id="+ str(id)
                cur.execute(qs)
            conn.commit()
        except ValueError as e:
            print("** ERROR creating table")
            print(e)
            cur.close()
            conn.close()

        cur.close()
        conn.close()


    def perform_query(self, query_str):
        conn = psycopg2.connect("dbname=" + self.dbname + " user=" + self.username)
        cur = conn.cursor()

        results = []
        r = []
        try:
            cur.execute(query_str)
            r = cur.fetchall() # This assumes the client performed pagination (limit/offset)

            # This is an attempt at generalizing a JSON wrapper for the column soup results of join hell.
            # Note that this only helps you so much on the JS front but it is at least enough to get you there.
            for p in r:
                o = {}
                for i in range(len(cur.description)):
                    d = cur.description[i]

                    # It's useful to wrap certain datatypes (datetime).
                    if isinstance(p[i], datetime):
                        o[d.name] = str(p[i])
                    else:
                        o[d.name] = p[i]
                results.append(o)

        except ValueError as e:
            print(e)
            cur.close()
            conn.close()
            return [] # This is wrong, but it never actually happens since Flask returns a 500 when anything throws.

        cur.close()
        conn.close()

        return results


def main():
    d = Dbcontrol()
    d.create_table_persons()
    d.create_table_items()
    d.create_table_purchases()
    d.create_persons()
    d.create_items()
    d.create_purchases()


if __name__ == '__main__':
    main()
